# Chapter 10 Notes


### Senate
* Senators for each State = 100 Senators
* 6 Year Terms
* No term limit
* Continuous body all of its seats are never up for election at the same time
***

### Total members of Congress = 535

***
### Qualifications for Senators
1. 30 years of age
2. Citizen of the U.S. for 9 years
3. must live in the state that elected in

Senators are always higher

***

### Committee Membership and public Servants
* Act as servants and answer to their constituents
* Most work done in committees

> Written with [StackEdit](https://stackedit.io/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTU4NTQ3OTc5NywyMDA5MjEyNTQ1XX0=
-->